## 1、mysql集群异常恢复

### 1.1、查看节点偏移量

```bash
cat /var/lib/mysql/grastate.dat

# 注意查看 seqno 的值
```

### 1.2、根据偏移量`seqno`的值来确定执行的操作

!!! tip

	**如果`seqno`有最大值则在最大值节点上执行如下命令：**<br><br>systemctl stop mysql<br>netstat -anp|grep 4567 (kill 掉监听4567的进程)<br>galera_new_cluster

!!! tip

    **如果`seqno`的值全部为`-1`,则任选一节点执行如下命令：**<br><br>systemctl stop mysql<br>netstat -anp|grep 4567 (kill 掉监听4567的进程)<br>galera_new_cluster<br><br>如果执行`galera_new_cluster`命令报错，则换一个节点执行`galera_new_cluster`命令，直到成功。

### 1.3、启动mysql集群

完成以上步骤后，在未执行`galera_new_cluster`命令的节点，执行以下操作：

```bash
systemctl start mysql
systemctl status mysql
```

### 1.4、查看mysql集群状态

在任一节点执行mysql，再执行`SHOW GLOBAL STATUS LIKE 'wsrep_%'`。

wsrep_incoming_addresses为三个节点则集群恢复成功。


## 2、计算节点业务网络配置

### 2.1、交换机上

!!! note

    创建聚合并配置

```bash
#
interface Bridge-Aggregation139
 port link-type trunk
 undo port trunk permit vlan 1
 port trunk permit vlan 2 to 4094
 link-aggregation mode dynamic
 vtep access port
#
```

!!! note

    将聚合添加到成员口，命令`port link-aggregation group 139`。

```bash
#
interface Ten-GigabitEthernet1/0/41
 port link-mode bridge
 description dev-arm-199.139
 port link-type trunk
 undo port trunk permit vlan 1
 port trunk permit vlan 2 to 4094
 port link-aggregation group 139
#

interface Ten-GigabitEthernet1/0/42
 port link-mode bridge
 description dev-arm-199.139
 port link-type trunk
 undo port trunk permit vlan 1
 port trunk permit vlan 2 to 4094
 port link-aggregation group 139
#
```

!!! note

    查看交换机邻居关系

```bash
dis lldp nei li

# 如果lldp建立成功，则可以查看到对端服务器的邻居关系，就像这样
[DEV1_Leaf1_96.3-Ten-GigabitEthernet1/0/41]dis lldp nei li
Chassis ID : * -- -- Nearest nontpmr bridge neighbor
             # -- -- Nearest customer bridge neighbor
             Default -- -- Nearest bridge neighbor
Local Interface Chassis ID      Port ID                    System Name
XGE1/0/1        b405-5d65-f21f  b405-5d65-f21f             -
XGE1/0/3        b405-5d65-f10d  b405-5d65-f10d             -
XGE1/0/4        b405-5d65-f49b  b405-5d65-f49b             DEV-VHST1-1004
XGE1/0/6        b405-5d65-eda7  b405-5d65-eda7             DEV-VHST1-1006
XGE1/0/7        b405-5d65-c5c1  b405-5d65-c5c1             DEV-VHST1-1007
XGE1/0/11       b405-5d65-f21e  b405-5d65-f21e             DEV-CTST1-1001
XGE1/0/13       b405-5d65-f10c  b405-5d65-f10c             DEV-CTST1-1003
XGE1/0/14       b405-5d65-f49a  b405-5d65-f49a             DEV-VHST1-1004
XGE1/0/16       b405-5d65-eda6  b405-5d65-eda6             DEV-VHST1-1006
XGE1/0/17       b405-5d65-c5c0  b405-5d65-c5c0             DEV-VHST1-1007
```

### 2.2、计算节点上

```python
# 检查ovs
ovs-vsc show
# 查看br-ex的bond和成员口
cat /etc/sysconfig/network-scripts/ifcfg-bond2
# 查看bond口配置，确认lacp配置如下
DEVICE=bond2
ONBOOT=yes
DEVICETYPE=ovs
TYPE=OVSBond
OVS_BRIDGE=br-ex
BOOTPROTO=none
BOND_IFACES="enP1p3s0f0 enP1p3s0f1"
OVS_OPTIONS="bond_mode=balance-tcp lacp=active other_config:lacp-fallback-ab=true other_config:lacp-time=fast"
```

## 3、ARM机器LLDP规避方法

```python
# 获取设备id
lspci | grep X710|awk  '{print $1}'

# 将设备id替换，执行如下命令
echo "lldp stop" > /sys/kernel/debug/i40e/0000\:06\:00.0/command
echo "lldp stop" > /sys/kernel/debug/i40e/0000\:06\:00.1/command

echo "lldp stop" > /sys/kernel/debug/i40e/0001\:03\:00.0/command
echo "lldp stop" > /sys/kernel/debug/i40e/0001\:03\:00.1/command
echo "lldp stop" > /sys/kernel/debug/i40e/0001\:05\:00.0/command
echo "lldp stop" > /sys/kernel/debug/i40e/0001\:05\:00.1/command
```

## 4、存储网络配置查看

```python
# 首先查看fc设备状态
cat /sys/class/fc_host/host1*/port_state

# 然后查看fc设备名称
cat /sys/class/fc_host/host1*/port_name

# 最后登录fc交换机查看链接状态
switchshow
```

## 5、门户升级后后端服务502的解决办法

!!! note

    重启门户&虚拟化前端即可

```python
# 查询前端的pod
kubectl get pod -n portal

# 重启前端的pod
kubectl delete pod portal-web-xxx -n portal
kubectl delete pod virtual-web-xxx -n portal
```

## 6、根目录占满如何清理？

首先`cd /`，然后使用命令：`du -h -x --max-depth=1`查看各个文件夹的大小，找到异常的文件夹和文件，针对性删除即可。

## 7、Linux bond 如何配置？

假设要将`enp1f0`和`enp1f1`两个口配置成负荷分担的`bond0`

首先，在`/etc/sysconfig/network-scripts/`下创建`ifcfg-bond0`配置文件，然后向其中键入以下内容：

```bash
TYPE=Ethernet
BOOTPROTO=none
NAME=bond0
DEVICE=bond0
ONBOOT=yes
BOND_IFACES="enp1f0 enp1f1"
BONDING_MASTER=yes
BONDING_OPTS="mode=4 miimon=100 lacp_rate=1 xmit_hash_policy=1"
```

接着，修改`ifcfg-enp1f0`和`ifcfg-enp1f0`为如下样子：

```bash
# ifcfg-enp1f0
BOOTPROTO=none
NAME=enp1f0
DEVICE=enp1f0
ONBOOT=yes
MASTER=bond0
SLAVE=yes

# ifcfg-enp1f1
BOOTPROTO=none
NAME=enp1f1
DEVICE=enp1f1
ONBOOT=yes
MASTER=bond0
SLAVE=yes
```

最后，我们重启网络服务`systemctl restart network` | `service network restart`即可。

## 8、Linux vlan子接口如何配置

### 8.1、网卡的vlan子接口

假设要在网卡`enp1f0`上创建vlan为123的子接口

首先，在`/etc/sysconfig/network-scripts/`下创建`ifcfg-enp1f0.123`配置文件，然后向其中键入以下内容：

```bash
TYPE=Vlan
PHYSDEV=enp1f0
BOOTPROTO=static
NAME=enp1f0.123
DEVICE=enp1f0.123
ONBOOT=yes
VLAN=yes
VLAN_ID=123
```

最后，使用命令`ifup enp1f0.123`拉起`enp1f0.123`即可。

### 8.2、bond的vlan子接口

和网卡的vlan子接口类似，但是前提是要先创建好bond

假设要在`bond0`上创建vlan为456的子接口

首先，在`/etc/sysconfig/network-scripts/`下创建`ifcfg-bond0.456`配置文件，然后向其中键入以下内容：

```bash
TYPE=Vlan
PHYSDEV=bond0
BOOTPROTO=static
NAME=bond0.456
DEVICE=bond0.456
ONBOOT=yes
VLAN=yes
VLAN_ID=456
```

最后，使用命令`ifup bond0.456`拉起`bond0.456`即可。


### 8.3、给vlan子接口添加IP

如果想要在vlan子接口上配置IP，配置增加如下三条即可：

```bash
IPADDR=1.2.3.4
NETMASK=255.255.255.0
GATEWAY=1.2.3.254
```

## 9、查看Mysql连接数

```bash
# 需要密码时
mysql -u -root -e "show status like '%conn%';" 

# 不需密码时
mysql  -e "show status like '%conn%';"
```

## 10、查看Mysql的链接对象

当我们的Mysql连接被打满时，我们可能需要查找具体是哪个服务或者进程占用了Mysql的连接，此时就可以用以下命令进行查询。

```bash
# 需要密码时
mysql -u -root -e "show processlist;" | grep nova | wc -l

# 需要密码时
mysql -e "show processlist;" | grep nova | wc -l
```

## 11、ipmitools命令

```bash
# 查询本机BMC IP
ipmitool lan print 1

# 查询本机电源状态
ipmitool power status
ipmitool power on
ipmitool power off
ipmitool power reset

# 查询其他机器
ipmitool –H (BMC的管理IP地址) –I lanplus –U (BMC登录用户名) –P (BMC 登录用户名的密码) lan print 1
```