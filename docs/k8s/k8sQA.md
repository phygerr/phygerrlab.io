## 1、如何在容器启动前自定义操作

最优雅的方式莫过于使用`initContainer`了，假设我们想要在主容器启动前通过命令或者Api获取一些数据，修改一些配置等，我们都可以选择使用`initContainer`来实现。

```yaml
apiVersion: v1
kind: Pod
metadata:
  name: mypod
spec:
  containers:
  - name: main-container
    image: my-image
    command:
    - start_app  # 这里是主容器的命令
  # 这里是init容器的操作
  initContainers:
  - name: init-script
    image: busybox
    command: ["sh", "-c", "echo 'Running init script...' && /path/to/init-script.sh"]
    volumeMounts:
    - name: shared-data
      mountPath: /path/to/shared-data
  - name: another-init-script
    image: busybox
    command: ["sh", "-c", "echo 'Running another init script...' && /path/to/another-init-script.sh"]
    volumeMounts:
    - name: shared-data
      mountPath: /path/to/shared-data
  volumes:
  - name: shared-data
    emptyDir: {}
```

## 2、经过ingress上传文件 413 Request Entity Too Large

修改对应的ingress的yml，在annotations:下增加proxy-body-size即可。

```yaml
apiVersion: networking.k8s.io/v1
kind: Ingress
metadata:
  annotations:
    kubectl.kubernetes.io/last-applied-configuration: |
      {"apiVersion":"networking.k8s.io/v1","kind":"Ingress","metadata":{"annotations":{}}
    nginx.ingress.kubernetes.io/proxy-body-size: 100m
  generation: 1
  name: test-ingress
```